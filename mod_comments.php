<?php
/**
 * @version    3.9.0
 * @package    Comments (for Joomla)
 * @author     Amany Abolella
 * @EMAIL      eng.ama.aboelella@gmail.com
 */

// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

$input=new JInput();
$check=$input->get('check', null);
if($check== null){
$layout = $params->get('layout','default');
$comments = ModCommentsHelper::listComments();
require JModuleHelper::getLayoutPath('mod_comments',$layout);
}
else{
    $save = ModCommentsHelper::saveComment($params);
    $layout = $params->get('layout','default');
$comments = ModCommentsHelper::listComments();
require JModuleHelper::getLayoutPath('mod_comments',$layout);
}