<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// no direct access
class ModCommentsHelper {

    /**
     * Retrieves the comments
     *     *
     * @access public
     */
    public static function listComments($comment_id = 0) {
//        var_dump($comment_id);die;
        // Obtain a database connection
        $db = JFactory::getDbo();
        // Retrieve the shout
        $query = $db->getQuery(true)
                ->select('*')
//                ->select(array('a.*', 'b.*'))
//                ->join('LEFT', $db->quoteName('#__modcomments_likes', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.comment_id') . ')')
                ->where($db->quoteName('parent_comment_id') . '=' . $comment_id)
                ->from($db->quoteName('#__modcomments'));

        // Prepare the query
        $db->setQuery($query);
        // Load the row.
        $result = $db->loadObjectList();
        // Return the comments
        return $result;
    }
    
    public static function likesNum($comment_id = null) {
        if(is_null($comment_id)){
            $totalLikes=0;
      
        }else{
           $db = JFactory::getDbo();
        // Retrieve the shout
        $query = $db->getQuery(true)
                ->select('SUM('.$db->quoteName('like').') as total')
                ->where($db->quoteName('comment_id') . '=' . $comment_id)
                ->from($db->quoteName('#__modcomments_likes'));

        // Prepare the query
        $db->setQuery($query);
        // Load the row.
        $totalLikes = $db->loadResult(); 
        }
        // Return the comments
        return $totalLikes;
    }

    public static function saveComment($params) {

        $input = new JInput();
        $parent = $input->get('comment_id', 0);
        $name = $input->get('name', null);
        $msg = $input->get('comment', null);
        if ($name == null || $msg == null) {
            return false;
        } else {
            // Create and populate an object.
            $data = new stdClass();
            $data->parent_comment_id = $parent;
            $data->comment = $msg;
            $data->comment_sender_name = $name;
            // Insert the object into the user profile table.
            $result = JFactory::getDbo()->insertObject('#__modcomments', $data);
        }
    }

    public static function getAjax() {

        $app = JFactory::getApplication()->input;
        $comment_id = $app->getString('Itemid');
        $like = $app->getString('like_unlike');
        // Create and populate an object.
        $data = new stdClass();
        $data->member_id = 1;
        $data->comment_id = $comment_id;
        $data->like = $like;
        // Insert the object into the user profile table.
        $result = JFactory::getDbo()->insertObject('#__modcomments_likes', $data);
        
        $db = JFactory::getDbo();
        // Retrieve the shout
        $query = $db->getQuery(true)
                ->select('SUM('.$db->quoteName('like').') as total')
                ->where($db->quoteName('comment_id') . '=' . $comment_id)
                ->from($db->quoteName('#__modcomments_likes'));

        // Prepare the query
        $db->setQuery($query);
        // Load the row.
        $totalLikes = $db->loadResult();
        return $totalLikes;
 
    }

}
