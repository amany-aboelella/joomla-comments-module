<?php

function img() {
	echo JURI::root().'modules/mod_comments/tmpl/assets/';
}
?>
   
<?php 
if(count($comments) >0){?>
   
<div id="output">
    <ul class="outer-comment">
        <?php foreach($comments as $comment){
            $id=$comment->id;
            ?>
        <li>                                        
            <div class="comment-row"> 
                <span aria-hidden="true"><img class="avatar" alt="<?php echo $comment->comment_sender_name; ?>"  src="<?php img(); ?>avatar.png"></span>
                <div class="comment-info">                                                
                    <span class="commet-row-label">from</span>                                                
                    <span class="posted-by"><?php echo $comment->comment_sender_name; ?></span>                                                
                    <span class="commet-row-label">at</span>                                                 
                    <span class="posted-at"><?php echo $comment->date; ?></span>                                            
                </div>                                            
                <div class="comment-text"><?php echo $comment->comment; ?></div>                                            
                <div>                                                
                    <a class="btn-reply" onclick="postReply(<?php echo $comment->id;?>)">Reply</a>                                            
                </div>                                            
                  <div class="post-action"> 
                    <?php 
                    $likes = ModCommentsHelper::likesNum($comment->id);
                         if($likes > 0){   
                            ?>
            
                    <img src="<?php img(); ?>like.png" id="unlike_<?php echo $comment->id;?>" class="like-unlike" onclick="likeOrDislike(<?php echo $comment->id;?>, -1)">
                    <img style="display:none;" src="<?php img(); ?>unlike.png" id="like_<?php echo $comment->id;?>" class="like-unlike" onclick="likeOrDislike(<?php echo $comment->id;?>, 1)">                                               
                    <span id="likes_<?php echo $comment->id;?>"><?php echo $likes;?> likes </span> 
                         <?php }else{ ?>
                    <img style="display:none;" src="<?php img(); ?>like.png" id="unlike_<?php echo $comment->id;?>" class="like-unlike" onclick="likeOrDislike(<?php echo $comment->id;?>, -1)">
                    <img src="<?php img(); ?>unlike.png" id="like_<?php echo $comment->id;?>" class="like-unlike" onclick="likeOrDislike(<?php echo $comment->id;?>, 1)">                                               
                    <span id="likes_<?php echo $comment->id;?>"> No  likes </span> 
                         <?php } ?>
                </div>                                       
            </div>
            <?php $replies = ModCommentsHelper::listComments($comment->id);
            if(count($replies)>0){
            ?>
            <ul>
                <?php foreach($replies as $reply){
                    ?>
                <li>                                        
                        <div class="comment-row">  
                            <span aria-hidden="true"><img class="avatar" alt="<?php echo $comment->comment_sender_name; ?>"  src="<?php img(); ?>avatar.png"></span>
                            <div class="comment-info">                                                
                                <span class="commet-row-label">from</span>                                                
                                <span class="posted-by"><?php echo $reply->comment_sender_name; ?></span>                                                
                                <span class="commet-row-label">at</span>                                                 
                                <span class="posted-at"><?php echo $reply->date; ?></span>                                            
                            </div>                                            
                            <div class="comment-text"><?php echo $reply->comment; ?></div>                                            
                            <div>                                                
                                <a class="btn-reply" onclick="postReply(<?php echo $reply->id;?>)">Reply</a>                                            
                            </div> 
                            <div class="post-action"> 
                    <?php 
                    $likes = ModCommentsHelper::likesNum($reply->id);
                         if($likes > 0){   
                            ?>
            
                    <img src="<?php img(); ?>like.png" id="unlike_<?php echo $reply->id;?>" class="like-unlike" onclick="likeOrDislike(<?php echo $reply->id;?>, -1)">
                    <img style="display:none;" src="<?php img(); ?>unlike.png" id="like_<?php echo $reply->id;?>" class="like-unlike" onclick="likeOrDislike(<?php echo $reply->id;?>, 1)">                                               
                    <span id="likes_<?php echo $reply->id;?>"><?php echo $likes;?> likes </span> 
                         <?php }else{ ?>
                    <img style="display:none;" src="<?php img(); ?>like.png" id="unlike_<?php echo $reply->id;?>" class="like-unlike" onclick="likeOrDislike(<?php echo $reply->id;?>, -1)">
                    <img src="<?php img(); ?>unlike.png" id="like_<?php echo $reply->id;?>" class="like-unlike" onclick="likeOrDislike(<?php echo $reply->id;?>, 1)">                                               
                    <span id="likes_<?php echo $reply->id;?>"> No  likes </span> 
                         <?php } ?>
                </div>          
                            </div>                                        
                        
                    <?php $replies = ModCommentsHelper::listComments($reply->id);
            if(count($replies)>0){
            ?>
            <ul>
                <?php foreach($replies as $reply){
                    ?>
                <li>                                        
                        <div class="comment-row"> 
                            <span aria-hidden="true"><img class="avatar" alt="<?php echo $comment->comment_sender_name; ?>"  src="<?php img(); ?>avatar.png"></span>
                            <div class="comment-info">                                                
                                <span class="commet-row-label">from</span>                                                
                                <span class="posted-by"><?php echo $reply->comment_sender_name; ?></span>                                                
                                <span class="commet-row-label">at</span>                                                 
                                <span class="posted-at"><?php echo $reply->date; ?></span>                                            
                            </div>                                            
                            <div class="comment-text"><?php echo $reply->comment; ?></div>                                            
                            <div>                                                
                                <a class="btn-reply" onclick="postReply(<?php echo $reply->id;?>)">Reply</a>                                            
                            </div> 
                            <div class="post-action"> 
                    <?php 
                    $likes = ModCommentsHelper::likesNum($reply->id);
                         if($likes > 0){   
                            ?>
            
                    <img src="<?php img(); ?>like.png" id="unlike_<?php echo $reply->id;?>" class="like-unlike" onclick="likeOrDislike(<?php echo $reply->id;?>, -1)">
                    <img style="display:none;" src="<?php img(); ?>unlike.png" id="like_<?php echo $reply->id;?>" class="like-unlike" onclick="likeOrDislike(<?php echo $reply->id;?>, 1)">                                               
                    <span id="likes_<?php echo $reply->id;?>"><?php echo $likes;?> likes </span> 
                         <?php }else{ ?>
                    <img style="display:none;" src="<?php img(); ?>like.png" id="unlike_<?php echo $reply->id;?>" class="like-unlike" onclick="likeOrDislike(<?php echo $reply->id;?>, -1)">
                    <img src="<?php img(); ?>unlike.png" id="like_<?php echo $reply->id;?>" class="like-unlike" onclick="likeOrDislike(<?php echo $reply->id;?>, 1)">                                               
                    <span id="likes_<?php echo $reply->id;?>"> No  likes </span> 
                         <?php } ?>
                </div>          
                            </div>                                        
                        
                    <ul>
                        
                    </ul>
                    </div>
                </li>
                <?php } ?>   
                    
            </ul>
            <?php } ?>  
                    </div>
                </li>
                <?php } ?>   
                    
            </ul>
            <?php } ?>   
        </li>
        <?php } ?>    
    </ul>
</div>
<?php } ?>