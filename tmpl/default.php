<?php 
// No direct access
defined('_JEXEC') or die; 
$document = JFactory::getDocument();
$document->addStyleSheet("/joomla/modules/mod_comments/tmpl/assets/style.css");
$document->addScript("/joomla/modules/mod_comments/tmpl/assets/jquery-3.2.1.min.js");
$document->addScript("/joomla/modules/mod_comments/tmpl/assets/comments_scripts.js");
?>
<div class="comment-form-container">
    <form id="frm-comment" name="comments_form" method="post">
        <fieldset>
        <div class="input-row">
            
            <input class="input-field"
                   type="text" name="name" id="name" placeholder="Name" />
        </div>
        <div class="input-row">
            <textarea class="input-field" type="text" name="comment"
                      id="comment" placeholder="Add a Comment">  </textarea>
        </div>
        <div>
            <button type="submit" class="btn-submit" id="submitButton">Publish</button>
                   
            
        </div>
        </fieldset>
        <input type="hidden" name="comment_id" id="commentId"/> 
        <input type="hidden" name="check" value="1"/> 
    </form>
</div>
<?php include 'list_comments.php';?>
        